# About Workplace

Smarter.Codes is a 35+ [remote first team](https://memory.ai/timely-blog/remote-first-company-culture#:~:text=A%20remote%2Dfirst%20culture%2C%20on,employees%2C%20or%20as%20a%20benefit.) <sup>5 min read</sup>

## Tools

As outlined in above article, we need unique tools to solve our unique problems. Some Unique problems such as
* Transparency on who is working, and on what. So that contributors Trust each other for 'doing the work'
* Asyncronous Communication. So that contrbutors can stay autonomous.
* Virtual Bonding. So that [fun](https://gitlab.com/smarter-codes/guidelines/workplace/communication#fun) fosters among contributors

To solve above problems we already are using a host of tools. Like
* [Hubstaff.com](https://Hubstaff.com) - to track our time (and money earned) every week, and share with our colleagues on where our time is being spent
* [Airtable.com](https://airtable.com/tble8ki7D0Arj9ZJL/viwUG9QrM2w6ivm3t?blocks=hide) - to plan our time for this week, and share it with others on how we would spend our time
* [Wiki.smarter.codes](https://wiki.smarter.codes/) - to express what our company's [BHAG](https://blog.growthinstitute.com/scale-up-blueprint/bhag-big-hairy-audacious-goal) is. And share the [weekly/quarterly/yearly objectives](https://wiki.smarter.codes/objectives-in-2021/) that company has set, teams have set, and individuals have set for themselves.
* [MS Teams](https://teams.microsoft.com/) and [MS Outlook](https://outlook.office.com/) - to instant message, and email anyone in team.
* [Gitlab.com](http://gitlab.com/) or [Trello.com](https://trello.com/) - to share requirements of work, and assign tasks to each other

It is important that the above be used in a weekly routine by you, to stay as a part of Smarter.Codes. If you are new at Smarter.Codes, it is important
that the team members onboarding you they involve you such you start using the above tools in a weekly routine.
3
There are more tools which are work in progress, and would soon be embraced by our team such as
* [Zoho Books](https://books.zoho.com/) which we would use to share finacials of ourselves [openly](https://en.wikipedia.org/wiki/Open_business).
* A [cap table management software](https://hackernoon.com/top-7-cap-table-management-softwares-you-can-try-in-2019-53a16371c353) which we would use to show how everyone in creation 'joint wealth' at Smarter.Codes

## Principles

[Embracing Open Business](#1)
